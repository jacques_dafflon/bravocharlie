//<!--- START DATEPICKER --->
    $(function(){
      $("#before_field").datepicker({ dateFormat : "dd/mm/yy" });
    });

    $(function(){
      $("#after_field").datepicker({ dateFormat : "dd/mm/yy" });
    });

//<!--- TOUR OPTIONS --->
    var trip = new Trip([
   {
       sel : '#terms',
       content : 'Input the terms of your search here.',
       position: "s",
       expose: true
   },
   {
       sel : '#before_field',
       content : 'Only display news published before this date.',
       position: "s",
       expose: true
   },
   {
      sel : '#after_field',
      content : 'Only display news published after this date.',
      position: "s",
      expose: true
   },
   {
      sel : '#after_field',
      content : 'If you insert both dates, only news in the range between them will be displayed.',
      position: "s",
      expose: true
   },
   {
      sel : '#after_field',
      content : 'We\'ll take care of finding which date is in the beginning and which one is in the end of the range.',
      position: "s",
      expose: true
   },
   {
      sel : '#after_field',
      content : 'You can also insert only dates if you want all the news for a specific period of time.',
      position: "s",
      expose: true
   },
   {
      sel : '#submit',
      content : 'Click here to submit your query.',
      position: "s",
      expose: true
   },
   {
      sel : '#submit',
      content : 'New are sorted from the most recent to the oldest.',
      position: "s",
      expose: true
   },
   {
      sel : '#submit',
      content : 'If you want all the terms to appear in the articles, join them with an &quot;AND&quot;.',
      position: "s",
      expose: true
   },
   {
      sel : '#querylanguage',
      content : 'Remember that you can use Lucene Query Language syntax for your queries!',
      position: "n",
      expose: true
   },
   {
      sel : '#querylanguage',
      content : 'The available fields are url, date, title and content. Dates must be formated as YYYYMMDD.',
      position: "n",
      expose: true
   }], {
    tripTheme : "white",
    showNavigation : true,
    showCloseBox : true,
    delay: 8000
  });

  //<!--- TOUR START --->
  var startTour = function(){
     trip.start();
  };

var handleInputValues = function(){
  var stats = [ ];
  var queryterms = $.url().param('q');
  if (queryterms) {
    $('#search_form').find("input[name='terms']").val(queryterms);
    stats.push('for query ' + '\'' + queryterms + '\'');
  }

  var beforedate = $.url().param('b') ? $.url().param('b') : $.url().param('rb');
  if (beforedate) $('#search_form').find("input[name='before']").val(parseDayFirst(beforedate));

  var afterdate = $.url().param('a') ? $.url().param('a') : $.url().param('re');
  if (afterdate) $('#search_form').find("input[name='after']").val(parseDayFirst(afterdate));

  if(beforedate && afterdate) stats.push('published between ' + prettyParseDate(beforedate) + ' and ' + prettyParseDate(afterdate));
  else if(beforedate) stats.push('published before ' + prettyParseDate(beforedate));
  else if(afterdate) stats.push('published after ' + prettyParseDate(afterdate));
  
  if(stats.length > 0)$('#statistics').append(stats.join(' '));
};

function parseDate(date) {
  return date === null ? null : moment(date).format("YYYY/MM/DD");
}

function parseDayFirst(date) {
  return date === null ? null : moment(date, 'YYYY/MM/DD').format("DD/MM/YYYY");
}

function prettyParseDate(date) {
  return date === null ? null : moment(date, 'YYYY/MM/DD').format("MMMM Do YYYY");
}

//<!---- ON DOCUMENT READY ----->
document.ready = function onReady() {

  //<!---- HANDLING INPUT VALUES ON GET REQUEST WITH QUERY PARAMS ----->
  handleInputValues();

  //<!---- AJAX POST REQUEST ----->
  $("#search_form").submit(function onSubmit(event){
    event.preventDefault();

    $('#loading').css('visibility','visible');
    var query = {};
    var $form = $(this);
    var new_url = [];  //Build the url to push after a search
    var stats = [ ];

    var terms = $form.find('input[name=\'terms\']').val();
    if (terms !== '') {
      query.q = terms;
      new_url.push('q=' + query.q);
      stats.push('for query ' + '\'' + query.q + '\'');
    }
    var before = parseDate($("#before_field").datepicker("getDate"));
    var after = parseDate($("#after_field").datepicker("getDate"));

    if (before !== null && after !== null) {
      //SWAPPING VALUES IF RANGE IS NEGATIVE, BOTH IN THE QUERY AND IN THE UI
      if(before > after){
        var temp = $("#before_field").datepicker("getDate");
        $("#before_field").datepicker("setDate", parseDayFirst($("#after_field").datepicker("getDate")));
        $("#after_field").datepicker("setDate", parseDayFirst(temp));
        before = parseDate($("#before_field").datepicker("getDate"));
        after = parseDate($("#after_field").datepicker("getDate"));
      }
      query.rb = before;
      query.re = after;
      new_url.push('rb=' + query.rb);
      new_url.push('re=' + query.re);
      stats.push('published between ' + prettyParseDate(query.rb) + ' and ' + prettyParseDate(query.re));
    } else if (before !== null) {
      query.b = before;
      new_url.push('b=' + query.b);
      stats.push('published before ' + prettyParseDate(query.b));
    } else if (after !== null) {
      query.a = after;
      new_url.push('a=' + query.a);
      stats.push('published after ' + prettyParseDate(query.a));
    }

    $.ajax({
      type: 'POST',
      url: $('#search_form').attr('action'),
      data: query,
      success: function onSuccess(data) {
        $('#results').empty().html(data);
        document.title = $('#statistics').html();
        if(stats.length > 0)$('#statistics').append(stats.join(' '));
        window.history.pushState(query, 'Title', '?' + new_url.join('&')); //URL MANIPULATION
        $('#loading').css('visibility','hidden');
      },
      error: function onError(error) {
        console.log(error);
        $('#results').empty().html('<div id="err_msg" class="center"><p>We are experiencing some problems,<br/>please reload the page.</p></div>');
        $('#loading').css('visibility','hidden');
      }
    });
    // window.onpopstate = function(e){  <---- THIS IS TO HANDLE FORWARD/BACK IN CASE WE WANT TO IMPLEMENT IT
    // if(e.state){
    //     document.getElementById('content').innerHTML = e.state.html;
    //     document.title = e.state.pageTitle;
    // }
    //};

  });
};
