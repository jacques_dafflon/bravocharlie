var moment     = require('moment');
var spawn      = require('child_process').spawn;
var dust       = require('dustjs-linkedin');
var spawn      = require('child_process').spawn;
var JSONStream = require('JSONStream');
var jsesc       = require('jsesc');

/*
 * GET home page.
 */
function home(req, res){
  var query = generateQuery(req.query);
  if (query.length > 0) {
    processQuery(query, function(err, result) {
      if (err) {
        res.render('newhome', {title: 'Error', err: 'Something went wrong while processing the query'});
        return;
        //throw err;
      }
      result.query = req.query;
      res.render('newhome', result);
      return;
    });
  } else if (query.length < 1 && Object.keys(req.query).length) {
    res.render('newhome', { title: 'Invalid query!', warn: 'Invalid query!'});
    return;
  } else {
    //RENDER HOMEPAGE
    res.render('newhome', { title: "Homepage" });
  }
}

/*
 * POST query
 */
function post(req, res) {
  var query = generateQuery(req.body);
  if (query.length < 1 && Object.keys(req.body).length) {
    res.render('news_list', { title: 'Invalid query!', warn: 'Invalid query!'});
    return;
  } else if (query.length < 1) {
    res.redirect(307, '/');
    return;
  }
  processQuery(query, function(err, result) {
    if(err){
      res.render('error_partial', { err: 'Something went wrong while processing the query'});
      return;
    }
    result.query = req.body;
    res.render('news_list', result);

  });
}

function generateQuery(query) {
  var queryString = [];
  var isQuery = query.q !== undefined;
  if (isQuery) {
    queryString.push("-query");
    queryString.push(/\s/.test(query.q) ? '"' + query.q + '"' : query.q);
  }
  var isBefore = query.b !== undefined && isValidDate(query.b);
  if (isBefore) {
    queryString.push("-before");
    queryString.push(query.b);
  }
  var isAfter = query.a !== undefined && isValidDate(query.a);
  if (isAfter) {
    queryString.push("-after");
    queryString.push(query.a);
  }
  var isRange = query.rb !== undefined && query.re !== undefined &&
                isValidRange(query.rb, query.re);
  if (isRange) {
    queryString.push("-range");
    queryString.push(query.rb);
    queryString.push(query.re);
  }
  console.log("GENRATED QUERY: ");
  console.dir(queryString);
  return queryString;
}

function isValidDate(date) {
  return moment(date, 'YYYY/MM/DD', true).isValid();
}

function isValidRange(a, b) {
  start = moment(a, 'YYYY/MM/DD', true);
  end = moment(b, 'YYYY/MM/DD', true);
  return start.isValid() && end.isValid() && end.diff(start, 'days') >= 0;
}

function processQuery(query, callback) {
  console.log('PROCESSING QUERY');
  var luceneCp = [ '',
    'analysis/common/lucene-analyzers-common-4.5-SNAPSHOT.jar:',
    'core/lucene-core-4.5-SNAPSHOT.jar:',
    'demo/lucene-demo-4.5-SNAPSHOT.jar:',
    'queryparser/lucene-queryparser-4.5-SNAPSHOT.jar:'
  ].join('./lucene/build/');
  var commonLang3Cp = [ '',
  '-javadoc.jar:',
  '-sources.jar:',
  '-tests.jar:',
  '.jar'
  ].join('./commons-lang3-3.1/commons-lang3-3.1');
  var options = [
    '-cp',
    luceneCp + commonLang3Cp,
    'org.apache.lucene.demo.SearchFiles',
    '-json'
  ];
  options.push.apply(options, query);

  var parser = JSONStream.parse();
  parser.on('error', function(err) {
    console.error("ERROR");
    console.dir(err);
    error = err;
    callback(err, null);
  });
  parser.on('root', function(obj) {
    for (var i in obj.news) {
      obj.news[i].date = moment(obj.news[i].date, 'YYYYMMDD')
        .format("MMMM Do YYYY");
    }
    console.log('FOUND ' + obj.length + " Results");
    if (obj.length < 1 && obj.warn === undefined) {
      obj.warn = 'No results found!';
    }
    obj.title = (obj.length > 0 ? "Results: " + obj.length : "No results found");

    callback(null, obj);
  });

  var searchFiles = spawn('java', options, { pipe: [0, 1, 2], cwd: '..' });
  searchFiles.stdout.pipe(parser); // No idea why can't be pased above...
  searchFiles.unref();
}

function returnData(errors, data, callback, deferred) {
  if (callback && typeof(callback) == "function") {
    callback(errors, data);
  } else {
    if (errors) {
      deferred.reject(errors);
    } else {
      deferred.resolve(data);
    }
  }
}

module.exports = {
  home : home,
  post : post
};