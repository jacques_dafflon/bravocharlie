.PHONY: nutch lucene node all run clean clean-nutch clean-lucene

NUTCH_RUN_DIR:=./nutch/runtime/local
LUCENE_CP:=./lucene/build/analysis/common/lucene-analyzers-common-4.5-SNAPSHOT.jar:./lucene/build/core/lucene-core-4.5-SNAPSHOT.jar:./lucene/build/demo/lucene-demo-4.5-SNAPSHOT.jar:./lucene/build/queryparser/lucene-queryparser-4.5-SNAPSHOT.jar:./commons-lang3-3.1/commons-lang3-3.1-javadoc.jar:./commons-lang3-3.1/commons-lang3-3.1-sources.jar:./commons-lang3-3.1/commons-lang3-3.1-tests.jar:./commons-lang3-3.1/commons-lang3-3.1.jar
ROUNDS:=2

all: clean-index install

crawl-index:
	${NUTCH_RUN_DIR}/bin/crawlindex ${NUTCH_RUN_DIR}/urls ${NUTCH_RUN_DIR}/crawl ${ROUNDS}

run:
	@echo && \
	echo "+--------------+" && \
	echo "| Running node |" && \
	echo "+--------------+"
	@cd ./node && node app.js

install: clean nutch lucene node

nutch:
	$(MAKE) -C ./nutch

lucene:
	$(MAKE) -C ./lucene/demo

node:
	@echo && \
	echo "+-------------------------+" && \
	echo "| Installing node mouldes |" && \
	echo "+-------------------------+"
	@cd ./node && npm install

clean: clean-nutch clean-lucene clean-node

clean-nutch:
	$(MAKE) -C ./nutch clean

clean-lucene:
	$(MAKE) -C ./lucene/demo clean

clean-node:
	@echo && \
	echo "+-----------------------+" && \
	echo "| Cleaning node mouldes |" && \
	echo "+-----------------------+"
	@cd ./node && rm -rf ./node_modules

clean-index:
	@echo && \
	echo "+-----------------------+" && \
	echo "| Cleaning Lucene Index |" && \
	echo "+-----------------------+"
	rm -rf ./index