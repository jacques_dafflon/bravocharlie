# BravoCharlie
A search engine for BBC news aimed on retrieval by date.

## Credits
* Simone D'Avico <simone.davico@usi.ch>
* Jacques Dafflon <jacques.dafflon@usi.ch>

## Dependencies
This project needs Node.Js v0.10.15 or higher to run the user interface.
You can get it [here](http://nodejs.org/). As well as the [express framework](http://expressjs.com/) which you can install by typing:

```$ npm install -g express```

This project also uses Nutch, Lucene and Apache Commons Lang but those are already provided.

For the client interface, we recommend using Google Chrome Version 31.0.1650.63 or higher.
Behaviour with older version or other web browsers is unpredicted.
You can get Chrome [here](www.google.com/chrome).

## Installation
Once you have satisfied all the dependencies, you can install the project by typing the following command from the root of the project:

```$ make install```

## Usage
Crawling and indexing is done together using the following command from the root of the project:

```$ make crawl-index ROUNDS=x```

Where ``x`` is the number of rounds Nutch has to do a crawling pass. 0 will skip the crawling and index the current crawl database. 1 Will only consider the seed and the existing crawl database. 2 or more will make multiple passes and crawl new pages.

To use the retrieval part, launch the Node.js server by typing the following command from the root of the project:

```$ make run```

The interface is then available at ``http://host:3000`` where ``host`` is your machine's IP address or hostname (by default ``127.0.0.1``).

Click on the "Take a tour!" button at the top right of your screen to understand how the project works.

### Two sites crawling
As discussed with the teaching assistant, we only crawl and index the BBC website and note CNN.
However we were supposed to demonstrate our ability to crawl two websites on November 25 2013 for the crawling demonstration. Since this demonstration did not happen, we have included a separate folder named ``nutch-two-sites`` which contains the revenant files we used to crawl to webiste with Nutch:
* ``seed.txt``: Contains the seeds.
* ``regex-urlfilter.txt`` : Filters the urls of the two sites unsung regular expressions.
* ``two_sites_dump`` : Sample Dump generated using notch with the two files above as settings.