package org.apache.lucene.demo;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.document.DateTools;

import java.io.*;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
public class IndexFiles {

  private static final String[] TOREMOVE = {
    "- You must have JavaScript enabled to see this content.",
    "Media playback is unsupported on your device",
    "Please turn on JavaScript. Media requires JavaScript to play."
  };

  private IndexFiles() {}

  /** Index all text files under a directory. */
  public static void main(String[] args) {
    String usage = "java org.apache.lucene.demo.IndexFiles"
                 + " [-index INDEX_PATH] [-dump DUMP_FILE] [-update]\n\n"
                 + "This indexes the dump in DUMP_PATH, creating a Lucene index"
                 + "in INDEX_PATH that can be searched with SearchFiles";
    String indexPath = System.getProperty("user.dir") + "/index";
    System.out.println("Index folder in " + indexPath);
    String dumpPath = null;
    for(int i=0;i<args.length;i++) {
      if ("-index".equals(args[i])) {
        indexPath = args[i+1];
        i++;
      } else if ("-dump".equals(args[i])) {
        dumpPath = args[i+1];
        i++;
      }
    }

    if (dumpPath == null) {
      System.err.println("Usage: " + usage);
      System.exit(1);
    }

    final File dumpFile = new File(dumpPath);
    if (!dumpFile.exists() || !dumpFile.canRead()) {
      System.out.println("Dump file '" +dumpFile.getAbsolutePath()+ "' does not exist or is not readable, please check the path");
      System.exit(1);
    }
    
    Date start = new Date();
    try {
      System.out.println("Indexing to directory '" + indexPath + "'...");

      Directory dir = FSDirectory.open(new File(indexPath));
      Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
      IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, analyzer);
      iwc.setOpenMode(OpenMode.CREATE);

      // Optional: for better indexing performance, if you
      // are indexing many documents, increase the RAM
      // buffer.  But if you do this, increase the max heap
      // size to the JVM (eg add -Xmx512m or -Xmx1g):
      //
      // iwc.setRAMBufferSizeMB(256.0);

      IndexWriter writer = new IndexWriter(dir, iwc);
      int documents = indexDump(writer, dumpFile);

      // NOTE: if you want to maximize search performance,
      // you can optionally call forceMerge here.  This can be
      // a terribly costly operation, so generally it's only
      // worth it when your index is relatively static (ie
      // you're done adding documents to it):
      //
      // writer.forceMerge(1);

      writer.close();

      Date end = new Date();
      System.out.println("Indexed " + documents + " documents in " + (end.getTime() - start.getTime()) + "ms.");

    } catch (IOException e) {
      System.out.println(" caught a " + e.getClass() +
       "\n with message: " + e.getMessage());
    }
  }

  static int indexDump(IndexWriter writer, File file) throws IOException {
    FileInputStream fis;
    try {
      fis = new FileInputStream(file);
    } catch (FileNotFoundException fnfe) {
      return 0;
    }

    int documents = 0;
    try {

      DataInputStream in = new DataInputStream(fis);
      BufferedReader br = new BufferedReader(new InputStreamReader(in));
      boolean skip = false;
      boolean removeTitle = false;
      String strLine = null;
      Document doc = null;
      String url = null;
      String title = null;
      String date = null;
      String content = null;
      String origContent = null;
      while ((strLine = br.readLine()) != null)   {
        if (strLine.length() == 0) {
          continue;
        }
        if (strLine.startsWith("Recno::") || skip) { // NEW DOCUMENT
          if (skip) { // SKIPPED DOCUMENT
            do {
              strLine = br.readLine();
              if (strLine == null) {
                break;
              }
            } while (!strLine.startsWith("Recno::"));
          } else if (doc != null) {
            if (url != null && title != null && date != null && content != null) { // INVALID DOCUMENT
              doc.add(new StringField("url", url, Field.Store.YES));
              doc.add(new StringField("title", title, Field.Store.YES));
              doc.add(new StringField("date", date, Field.Store.YES));
              doc.add(new LongField("modified", file.lastModified(), Field.Store.NO));
              doc.add(new TextField("content", content, Field.Store.YES));
              // Commit a new documet into Index
              System.out.println("Adding document");
              writer.addDocument(doc);
              documents++;
            } // Show what we indexed
            for (IndexableField field : doc.getFields()) {
              System.out.println(field.name().toUpperCase() + ": " + field.stringValue());
            }
            System.out.println("");
          } //Reset fields and doc.
          url         = null;
          title       = null;
          date        = null;
          content     = null;
          skip        = false;
          removeTitle = false;
          doc         = new Document();
        } else if (strLine.startsWith("URL::")) { // INDEX URL
          url = strLine.replace("URL:: ", "");
          if (url.equals("http://www.bbc.co.uk/news/")) {
            skip = true;
          }
        } else if (strLine.startsWith("Title: ")) { // INDEX TITLE
          title = strLine.replace("Title: ", "").replace("BBC News - ", "");
          if (removeTitle) {
            content = IndexFiles.processContent(content, title);
            if (content.length() < 120) { // Too short
              skip = true;
            }
            removeTitle = false;
          }
        } else if (strLine.startsWith("Parse Metadata: ")) { // INDEX DATE
          Pattern p = Pattern.compile("OriginalPublicationDate=(\\d\\d\\d\\d/\\d\\d/\\d\\d \\d\\d:\\d\\d:\\d\\d)");
          Matcher m = p.matcher(strLine);
          if (m.find()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            try {
              Date d = sdf.parse(m.group().replace("OriginalPublicationDate=", ""));
              date = DateTools.timeToString(d.getTime(), DateTools.Resolution.DAY);
            } catch (ParseException e) {
              System.out.println("Unable to parse date.");
              date = null;
            }
          }
        } else if (strLine.startsWith("ParseText::")) { // INDEX CONTENT
          strLine = br.readLine();
          origContent = strLine;
          if (strLine == null) {
            System.out.println("Invalid document: unexpected EOF!");
            break;
          }
          content = strLine.replace("BBC News - ", "");
          if (title != null) {
            content = IndexFiles.processContent(content, title);
            if (content.length() < 120) { // Too short
              skip = true;
            }
          } else {
            removeTitle = true; // We need to wai to get the title to process
          }
        }
      }
      in.close();
    } finally {
      fis.close();
    }
    return documents;
  }

  private static String processContent(String content, String title) {
    for (String toRemove : TOREMOVE) {
      content = content.replace(toRemove, "");
    }
    if (content.length() > title.length()+1 && content.startsWith(title)) {
      content = content.substring(title.length()+1);
    }
    if (content.startsWith(" ") && content.length() > 2) {
      content = content.substring(1);
    }
    return content.replace("  ", " ");
  }

  private static void printDocFields(String url, String title, String date,
      String content, String msg) {
    System.out.println(msg);
    System.out.println("URL: " + url);
    System.out.println("TITLE: " + title);
    System.out.println("DATE: " + date);
    System.out.println("CONTENT: " + content + "\n");
  }
}
