package org.apache.lucene.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.queryparser.flexible.core.parser.EscapeQuerySyntax;
import org.apache.lucene.queryparser.flexible.standard.parser.EscapeQuerySyntaxImpl;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

public class SearchFiles {

  private static final String ln = System.getProperty("line.separator");

  private SearchFiles() {}

  public static void main(String[] args) throws Exception {
    String usage = "java org.apache.lucene.demo.SearchFiles"
                 + " -query terms | -before YYYY/MM/DD | -after YYYY/MM/DD | -range YYYY/MM/DD YYYY/MM/DD [-json]"
                 + ln
                 + ln;
    String index      = "index";
    String[] fields   = { "content", "title" };
    String queries    = null;
    String queryInput = null;
    String before     = null;
    String after      = null;
    String range      = null;
    String rangeA     = null;
    String rangeB     = null;
    boolean json      = false;
    ArrayList<String> dates = new ArrayList<String>(3);


    Sort sort = new Sort(new SortField("date", SortField.Type.STRING, true));
    EscapeQuerySyntax escaper = new EscapeQuerySyntaxImpl();


    for(int i=0;i<args.length;i++) {
      if ("-query".equals(args[i])) {
        queryInput = escaper.escape(
          args[++i],
          new Locale("eng", "GB"),
          EscapeQuerySyntax.Type.STRING
        ).toString();

      } else if ("-before".equals(args[i])) {
        before = args[++i];
        if (!SearchFiles.isDate(before)) {
          System.out.println(usage);
          System.exit(1);
        }
        dates.add("date:{0 TO " + SearchFiles.toLQLDate(before) + "}");

      } else if ("-after".equals(args[i])) {
        after = args[++i];
        if (!SearchFiles.isDate(after)) {
          System.out.println(usage);
          System.exit(1);
        }
        dates.add("date:{" + SearchFiles.toLQLDate(after) + " TO 99999999}");

      } else if ("-range".equals(args[i])) {
        rangeA = args[++i];
        rangeB = args[++i];
        if (!(SearchFiles.isDate(rangeA) && SearchFiles.isDate(rangeB))) {
          System.out.println(usage);
          System.exit(1);
        }
        dates.add("date:[" + SearchFiles.toLQLDate(rangeA)
          + " TO " + SearchFiles.toLQLDate(rangeB) + "]");
      } else if ("-json".equals(args[i])) {
        json = true;
      }
    }

    String line = SearchFiles.buildQueryString(dates, queryInput);
    if ("".equals(line)) {
      System.out.println(usage);
      System.exit(1);
    } else {
      line = line.trim();
    }
    File file = new File(index);
    if (!file.exists()) {
      if (json) {
        System.out.println("{\"title\":\"No Index!\","
          + "\"err\":\"The index has not been created. "
          + "Index using Lucene first and then\"}");
        System.exit(1);
      } else {
        System.err.println("The index has not been created.");
        System.err.println("Index using Lucene first.");
      }
    }
    IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(index)));
    IndexSearcher searcher = new IndexSearcher(reader);
    Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);

    MultiFieldQueryParser parser = new MultiFieldQueryParser(
      Version.LUCENE_40, fields, analyzer);

    Query  query = null;
    try {
      query = parser.parse(line);
    } catch (ParseException e) {
      if (json) {
        System.out.println("{\"title\":\"Invalid Query!\","
          + "\"warn\":\"Invalid Query!\"}");
      } else {
        System.out.println("Invalid Query!");
        System.out.println(e.toString());
      }
      System.exit(1);
    }
    if (!json) {
      System.out.println("Searching for: " + query.toString());
    }

    TopDocs results = searcher.search(query, 1000, sort);
    ScoreDoc[] scoreDocs = results.scoreDocs;

    if (scoreDocs.length == 0) {
      System.out.println(json ? "{\"length\":0,\"news\":[]}" : "No such page");
    } else {
      System.out.println(json
        ? "{\"length\":" + scoreDocs.length + ",\"news\":["
        : ("Found " + scoreDocs.length + " documents"));
      for (int i = 0; i < scoreDocs.length; i++) {
        Document doc = searcher.doc(scoreDocs[i].doc);
        System.out.println(json
          ? SearchFiles.jsonResult(doc)
          : ( (i+1) + ". " + SearchFiles.formatResult(doc) ));
        if (json) {
          System.out.println(i != scoreDocs.length-1 ? "," : "]}");
        }
      }
    }
    reader.close();
  }

  private static boolean isDate(String date) {
    Pattern p = Pattern.compile("^\\d\\d\\d\\d/\\d\\d/\\d\\d$");
    Matcher m = p.matcher(date);
    return m.find();
  }

  private static String toLQLDate(String date) {
    return date.substring(0,4) + date.substring(5,7) + date.substring(8,10);
  }

  private static String formatDate(String LQLDate) {
    return LQLDate.substring(0,4) + "/" + LQLDate.substring(4,6) + "/" + LQLDate.substring(6,8);
  }

  private static String formatResult(Document doc) {
    return doc.get("title") + " - " + SearchFiles.formatDate(doc.get("date"))
      + System.getProperty("line.separator") +  "\t-> " + doc.get("url");
  }

  private static String jsonResult(Document doc) {
    return "{\"title\":\"" + escapeHtml4(doc.get("title"))
      + "\",\"date\":\"" + doc.get("date") + "\",\"url\":\"" + doc.get("url")
      + "\",\"content\":\"" + escapeHtml4(doc.get("content")) + "\"}";
  }

  private static String buildQueryString(ArrayList<String> dates, String queryInput) {
    if (dates.isEmpty()) {
      return queryInput == null ? "" : queryInput;
    } else {
      return "(" + StringUtils.join(dates, " OR ") + ")"
      + (queryInput == null ? "" : " AND " + queryInput);
    }
  }
}
