\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{dsfont}
\usepackage{graphicx}
%\usepackage[colorinlistoftodos]{todonotes}
\usepackage[usenames,dvipsnames]{color}
\usepackage[bookmarks=true, colorlinks=true, breaklinks, pdftitle={BravoCharlie Report},pdfauthor={Simone D'Avico, Jacques Dafflon}]{hyperref}%\usepackage{hyperref}

\definecolor{BBCRed}{RGB}{155,0, 12}
\hypersetup{linkcolor=BBCRed,citecolor=BBCRed,filecolor=BBCRed,urlcolor=[named]{NavyBlue}}
\newcommand{\engine}{\textit{BravoCharlie}}
\title{Multimedia Information Access and Retrieval \\ \engine\ Project Report}
\author{Simone D'Avico, Jacques Dafflon}

\begin{document}
\maketitle

\tableofcontents

\newpage

\section{Introduction}

In this project, we had to design, implement and evaluate a news search engine that allows users to search the BBC website. The system had to provide an interface for searching, browsing, and presenting the results. Moreover, the search engine (which we will call \engine\ from now on) had to feature the following functionalities:

\begin{itemize}

\item \textit{Range Search:} find documents published before or after a specific date, or in a specific interval.

\item Display relevant documents in a reverse chronological order based on the page creation date.

\item Queries must be interpreted using the Lucene Query Language\footnote{See \url{https://lucene.apache.org/core/2_9_4/queryparsersyntax.html}}.

\end{itemize}

\section{Implementation}

We wanted \engine\ to be effective and simple to use. In order to meet these two requirements, we spent a lot of time on choosing and tuning the technologies at out disposal, with the purpose of getting the best results from the crawling and the retrieval. Moreover, we wanted a modern and pleasant user interface, which would allow users to retrieve news easily and in a familiar way.

\subsection{Components and Technologies}


\begin{figure}[h]
\begin{center}
\includegraphics[width=1\textwidth]{./images/components.pdf}
\caption{\engine's main components.}
\end{center}
\end{figure}
\newpage
\subsubsection{Requirements}

The specifications for index creation follow the assignment's requirements:

\begin{enumerate}

\item Only news from the BBC are crawled, indexed, and retrieved.
\item \href{https://nutch.apache.org/}{Nutch 1.7} is used for crawling.
\item \href{https://lucene.apache.org/}{Lucene 4.5.0} is used for indexing.
\item Lucene 4.5.0 is used for retrieving documents.
\item \href{http://nodejs.org/}{Node.js 0.10.15} is used for the user interface.

\end{enumerate}

\paragraph{Note:} The user interface has been tested and works with Google Chrome, version \texttt{31.0.1650.63}. The behaviour of the system with older version of Google Chrome or other web browser is unpredicted.

\subsubsection{Crawling}
Crawling is done using Nutch. To crawl BBC news article, we use a default Nutch
system with the unique seed \texttt{\url{http://www.bbc.co.uk/news/}}. In order to crawl
only and correctly news articles on the BBC website we extended the Nutch configuration to ignore
external links (\texttt{db.ignore.external.links}) and to increase the number of maximum outlinks
per page (\texttt{db.max.outlinks.per.page}) from the default 100 to 200. Hence the crawling time is increased for each segment but yields better results.

Secondly, to restrict the crawler to news related pages on the BBC website,
we used regular expression as url filters to match the address of news articles.
Those urls are of the form:\\ \texttt{\ldots bbc.co.uk/news-<dash-spearated-words><numeric id>}. The following regular expression is thus used to get urls of that form:\\
\texttt{+\textasciicircum http://([a-z0-9]*\textbackslash.)*bbc\textbackslash.co\textbackslash.uk/news/(([a-zA-Z]+-)+([0-9]+))?\$}

Moreover, Nutch parses each page and extract content form it. To improve the quality of the content extraction, we decided to change from the default HTML parser to the Tika parser\footnote{See \url{https://tika.apache.org/}}. The Tika parser comes with \texttt{ArticleExtractor}, an extractor
tuned for news articles which achieves a higher accuracy than the the default extractor.

Finally, we also used the \texttt{Parse-metatags} plugin\footnote{See \href{https://issues.apache.org/jira/browse/NUTCH-809}{NUTCH-809}} to parse meta tags value in order to get the correct date of each news article. This also allows us to eliminate false positive during the indexing process as some pages with no articles do not have a publication date.

Finally once the crawling is finished, we produce a dump file which contains information such as the URL, title, date and parsed content of the crawled pages.

\subsubsection{Indexing}
Indexing is done using Lucene, with a custom implementation of the indexing process loosely based on the example given in class. Lucene will read the dump file created by
Nutch. For each document the following attributes will be extracted: the URL, the title, the date and the content.

Moreover, to improve the quality of the index and thus the retrieval process later on;
we filter each field specifically for each document.
\begin{description}
\item[URL:] If the URL matches the seed's URL (which we cannot ignore in Nutch), we do not index the document.
\item[date:] The date is extracted from the meta tags of the dump using a custom regular expression. If the date is not found, it means it is not a news article and the document is dropped from the index. In addition, the date is parsed
using the DateTools from the Lucene API\footnote{See \url{http://lucene.apache.org/core/4_5_0/core/org/apache/lucene/document/DateTools.html}}. This generates a string representation of the date suitable for indexing and which can be organized chronologically by sorting lexicographically.
\item[title:] A large quantity of the titles is parsed as ``BBC News --- \ldots''. For aesthetic, this part is truncated from the tile before indexing.
\item[content:] The content is given a lot of consideration before being indexed.
Similarly to the tile, it can start with ``BBC News --- \ldots'' or even with the entire title. This part is truncated.

In addition the length of the content is taken into consideration. Documents with very short indexes are not indexed as they represent
``summary'' pages on a topic. These pages are great for crawling as they contain a lot of outlinks to actual news articles. However they are not news article themselves and should no appear in the results.

Finally since Nutch does not support Javascript, messages such as: ``Please turn on JavaScript. Media requires JavaScript to play.'' are injected in the middle of articles.
We have generated a small list of such messages which we prune before indexing the content.
\end{description}

In the end this yields good results. We eliminate almost all false positives from the index and the actual articles have a cleaned up content which does not contain irrelevant messages related to technical details and not the article.

\subsubsection{Retrieving}
Retrieving is done using Lucene. The retrieval implementation is loosely based on the example seen in class. For compatibility with the rest of our system, a single query is passed as argument to Lucene which prints the result to the standard output and then exits. The following arguments are supported when submitting a query:
\begin{description}
\item[\texttt{-query ``query''}] This argument accepts a single value which is interpreted as a query expressed in the Lucene Query Language. The default fields for a query are the title (\texttt{title:\ldots}) and the content (\texttt{content:\ldots}). Obviously, the value is escaped into a Lucene query before being processed for security and stability reasons.
\item[\texttt{-before ``YYYY/MM/DD''}] This argument accepts a date as a string
which contains a date with the year (4 digits), the month (2 digits) and the day (2 digits) separated with forward slashes. This is applied only on the \texttt{date} field
during the query and will return the documents with an exclusively earlier date than the one provided.
\item[\texttt{-after ``YYYY/MM/DD''}] This argument accepts a date as a string
which contains a date with the year (4 digits), the month (2 digits) and the day (2 digits) separated with forward slashes. This is applied only on the \texttt{date} field
during the query and will return the documents with an exclusively later date than the one provided.

\item[\texttt{-range ``YYYY/MM/DD YYYY/MM/DD''}] This argument accepts two dates as strings
which contains each a date with the year (4 digits), the month (2 digits) and the day (2 digits) separated with forward slashes. This is applied only on the \texttt{date} field
during the query and will return the documents within the inclusive range expressed by the two date. If the second date is less than the first, nothing will be returned.

\item[\texttt{-json}] This is an optional argument which takes no value.
If given the output will be a valid JSON which can be parsed. In addition, both the title and the content will be valid HTML which is escaped using the Apache Commons Lang 3 libraries\footnote{See \url{https://commons.apache.org/proper/commons-lang/}}.
\end{description}
If more than one argument among \texttt{-before}, \texttt{-after}, \texttt{-range} is given; then the query will match the documents matching any one of the date constraints and not necessarily all of them. The user interface prevents this behaviour by default.

Note that at least one of \texttt{-query}, \texttt{-before}, \texttt{-after} or \texttt{-range} must be given.
The retrieval process also detects a missing index and will not simply crash.

\subsubsection{The Front-end}

We decided to implement the interface for the search engine by exploiting web technologies; thus, we implemented a small server in \texttt{node.js}, which handles the requests from the user interface (written in HTML5+CSS3 and JavaScript) and takes care of calling Lucene's \texttt{SearchIndex} and render the results.

When the user submits a query, the query is parsed on the \texttt{node.js} server into
valid arguments for the retrieval (\texttt{-query}, \texttt{-before}, \texttt{-after}, \texttt{-range}). In addition the dates are checked to be valid before attempting to retrieve documents.

Once the validity of the query has been verified,  a system call to Lucene is emitted from the server and the output is streamed back and parsed into a JSON containing the result of the query. Then a result is injected into a template which is rendered and sent to the client.

\subsection{The User Interface}

\subsubsection{The Search Bar}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{./images/search_bar.png}
\caption{The Search Bar}
\end{center}
\end{figure}

The search bar is placed at the top of the page as a fixed element, so that the user can quickly see and change query terms while he is scrolling results. The search bar features three fields:

\begin{enumerate}

\item The \textit{terms} field, where the user can specify the terms for the query;
\item The \textit{before} date field, where the user can specify a date and retrieve all the documents created before that date;
\item The \textit{after} date field, where the user can specify a date and retrieve all the documents created after that date; in addition, if the user specifies both dates, they will be used as a range for the Range Search feature.

\end{enumerate}

Since the possibility to restrict search results by date is a main feature of our search engine, we wanted to provide an easy way to input dates, so that users wouldn't have to bother inserting them by hand, which is a slow and error-prone process. Thus, the user interface includes date-pickers (Figure \ref{datepicker}) to choose dates quickly and easily.

\begin{figure}[hbtp]
\begin{center}
\includegraphics[scale=0.5]{./images/datepicker.png}
\caption{The Datepicker}
\end{center}
\label{datepicker}
\end{figure}

\subsubsection{The Results}

We designed the results list (Figure \ref{newslist}) to resemble as much as possible the style of the most used search engines. Each box represents a retrieved news, and features the title of the news, the clickable URL, the date of publication of the article and a snippet from the article. Moreover, the list features the number of retrieved articles.

\begin{figure}[hbtp]
\begin{center}
\includegraphics[scale=0.35]{./images/news_list.png}
\caption{The Retrieved Results}
\label{newslist}
\end{center}
\end{figure}

\subsubsection{Focusing on Usability}

While we were developing the search engine we asked ourselves what were the features we wanted it to have, in case we had to use it for real. This led to the implementation of several features that are strictly related to usability: for example, a user may want to send the results of his search to another user; this is usually done by copy pasting and sending the search engines' URL. We implemented this feature in our search engine, such that the query string of the URL changes in reaction to a user query and can be used later to perform the same query quickly. For example, if  a user searches for ``Mandela" in a range from 01/12/2013 to 25/12/2013, the URL will be changed to:

\begin{center}
\texttt{http:\//\//localhost:3000\//?q=mandela\&rb=01\//12\//2013\&re=25\//12\//2013}
\end{center}

This URL can be later used to perform a request to the search engines with the same query.

The query string of the URL supports the following fields:
\begin{description}
\item[\texttt{q}] The value is the query terms using the Lucene Query Language.
\item[\texttt{b}] The value is a date as string with the format  \texttt{YYYY/MM/DD}. Only news published before this date will be returned.
\item[\texttt{a}] The value is a date as string with the format  \texttt{YYYY/MM/DD}. Only news published after this date will be returned.
\item[\texttt{rb}] The value is a date as string with the format  \texttt{YYYY/MM/DD}. It indicates the beginning date of a range search and must be sent along with the \texttt{re} field.
\item[\texttt{re}] The value is a date as string with the format  \texttt{YYYY/MM/DD}. It indicates the end date of a range search and must be sent along with the \texttt{rb} field.
\end{description}

\section{Evaluation}

\subsection{Procedure}
In order to perform an evaluation of \engine\ we designed a simple set of tasks that involved retrieving articles by using \engine's main features:

\begin{enumerate}

\item Take the guided tour to learn how to search with \engine.
\item Do you know the Lucene Query Language?
\item Search for news about Mandela's death;
\item Search for news about Mandela before 15/12/2013;
\item Search for news about Mandela after 15/12/2013;
\item Search for news on Mandela between 05/12/2013 and today;
\item Search for today's news.

\end{enumerate}

Before completing these tasks, we asked the user the following question:

\begin{enumerate}
\item Are you familiar with the Lucene query language?
\end{enumerate}

After completing these tasks, the user had to evaluate his experience with the search engine by expressing appreciation for the engine's features with a number from 1 to 10:

\begin{enumerate}


\item Do you like the interface's design?
\item How intuitive is it?
\item Did the system display your search results fast enough?
\item Was it easy to search documents within certain date ranges?
\item Do you like how search results are presented?
\item Was the guided tour useful? How much?

\end{enumerate}

\subsection{Outcome}
We have performed this evaluation on three members of our class as well as a master student. Based on their feedback, we have drawn the following conclusions:

\begin{description}
\item[Interface] The users were generally pleased with the interface. The one negative feedback was the way the dates were displayed in the input field (with the month first).
\item[Ease of use] The interface was intuitive, the users had no problem moving around, inputting queries,
retrieving result and clicking on them.
Some users had trouble searching for ``todays news''. Moreover searching for news within a range revealed some users had some doubts as to where to insert the beginning and end dates, despite the explanation during the tour.
\item[Speed] The system has not been optimized or tested under heavy load and the index was small at the time of the test (about 500 documents), however users were pleased with the speed at which results were displayed.
\item[Results] Users were pleased as to how the individual results were displayed.
On the other hand most user were displeased with the results being displayed in a reverse chronological order. This was even more frustrating considering since by using the Lucene Query Language, the following query: \texttt{``Mandela Death''} is interpreted as \texttt{``Mandela OR Death''}. Thus a less relevant result (containing only one of the two terms) but more recent would be displayed first.
To correct such a problem and match both terms, the user has to input a query such as
\texttt{``Mandela AND Death''} or \texttt{``+Mandela +Death''}.
\item[Guided Tour] The guided tour was appreciated by the users. Some of the explanations however were a bit long. In addition it seemed like users were not very concentrated during the tour.
\end{description}
\subsection{Threats to Validity}
The first thing to take into consideration is the scale of this evaluation which was extremely small. We had a sample size of four people.
The next obvious problem with such a sample is the lack of diversity. All of our candidates are informatics students of the Universit\`{a} della Svizzera italiana.
Three of them are currently developing similar systems.

Moreover, none of our candidates matched the main users for the system.
Indeed the system focuses on news from a British website and none of our candidates were from the United Kingdom. The British culture, even though somewhat similar to the culture of our candidates, is slightly different and might raise problems which went undetected during our evaluation.

Finally, the candidates had to perform only seven tasks and answer seven questions. Given such a small evaluation, it is highly likely that some aspects might have passed undetected and only a more extensive evaluation would reveal them.
\subsection{Improvements}
\begin{enumerate}
\item Based on the experimentation, the following improvements have been implemented.
The explanation regarding range search in the tour has been clarified.
\item Because displaying the news in a reverse chronological order is a requirement we were not able to change the way the news are displayed. Nonetheless, we have added some information about it during the tour to warn the user. Moreover a message is now displayed if no results are found to remind the user to take the tour or consult the documentation of the Lucene Query Language in order to perform better queries.
\end{enumerate}

\newpage
\section{Closing Remarks}
The system meets all the requirements required.
The crawling and indexing revealed to be more than satisfactory. The number of false positives and false negatives are low. Moreover, very few fetch operation actually timeout. Nonetheless given the means and time, using technologies such as Apache Hadoop would provide a clear improvement on the crawling efficiency. In addition, since we are only crawling a single site, implementing a custom crawler could improve both performances and the quality of the parsing. With the drawback that it needs to be maintained, to adapt to the changes the BBC makes to its website.

Indexing is one of the most satisfactory part of the project and performed very well, the code could be optimized in order to index faster. Otherwise one of the possible extension would be the generation of tags related to the document based on the content.
However this could only be useful if an option to sort by relevance is offered.

Retrieval could be optimized by streaming the results as they come instead of sending them in block. Another clear improvement would be to offer the possibility to retrieve the news sorted by relevance and not reversed chronological order

The interface is quite satisfying based on the users' evaluation. However a nice improvement would be to include some query preprocessing in order to have results which match more closely the users' expectations and does not break the principle of least astonishment (POLA).

In conclusion we can see that the system can be easily extend in many different directions depending on the requirements: performance, effectiveness, usability, and so forth.
Nonetheless, at the current states, the system is easy to use, performs well and users were satisfied with it, which was our main goal.
\end{document}