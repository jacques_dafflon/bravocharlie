\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Implementation}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Components and Technologies}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Requirements}{3}{subsubsection.2.1.1}
\contentsline {paragraph}{Note:}{3}{section*.2}
\contentsline {subsubsection}{\numberline {2.1.2}Crawling}{3}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Indexing}{4}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Retrieving}{4}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}The Front-end}{5}{subsubsection.2.1.5}
\contentsline {subsection}{\numberline {2.2}The User Interface}{6}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}The Search Bar}{6}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}The Results}{6}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Focusing on Usability}{7}{subsubsection.2.2.3}
\contentsline {section}{\numberline {3}Evaluation}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Procedure}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Outcome}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Threats to Validity}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Improvements}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Closing Remarks}{10}{section.4}
